import { defineStore } from "pinia";

export const useUserStore = defineStore("token", {
  state: () => ({
    loggedInUser: null,
  }),

  actions: {
    async saveUserInStore(username) {
        try{
            this.loggedInUser = username
        } catch (err){
            console.log(err)
        }
    }
  },
});
