# Goals
In this session, we will learn the following main concepts.

- Create and run sample Vue.js application
- Review of Vuejs Application 
- Write new and reusable components
- Basic styling of components


## Prerequisite
Make sure that you have an up-to-date version of Node.js (https://nodejs.org) installed.

If you need multiple versions of Node.js in you machine, you can use Node Version Manager (https://github.com/nvm-sh/nvm).

You can check the current version of Node.js in your machine using 

```
node -v
```

# Session I - Introduction

## Create and run a first Vue.js project

A sample Vue.js project can be created using this command from your workspace directory.

```
npm create vue@latest
```

This command will install and execute create-vue, the official Vue project scaffolding tool.
You will then be asked various options and following snippet shows the the recommended options to use! Checkout generated README.md file for more details -  for example 'Recommended IDE Setup' section.


```
npm create vue@latest
Need to install the following packages:
  create-vue@3.9.1
Ok to proceed? (y) 

Vue.js - The Progressive JavaScript Framework

✔ Project name: … 01-vuejs-introduction
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add an End-to-End Testing Solution? › Cypress
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in <your workspace directory>/01-vuejs-introduction...

Done. Now run:

  cd 01-vuejs-introduction
  npm install
  npm run format
  npm run dev
```

Run the generated app by following the instructions displayed in the console! Basically, it can be done in two steps.

- Install the project dependencies using the command ```npm install ```. Note that this command need to be executed for the first time, and everytime when the project dependencies changes. The project dependencies are listed/configured in package.json file.
- Run the project using the command ```npm run dev```. Under the hood, this command calls ```vite``` command, see scripts section in package.json file. Note that vite (https://vitejs.dev) is a platform-agnostic frontend tool for building web applications. 

By now, you should be able to create a project and run it.


## Understand the parts of generated project

Inspect the generated app, especially understand the folling points

 - The project structure
 - How index.html in the root folder maps the main (entry) JavaScrit file (/src/main.ts) and how that maps the entry Vue.js component (src/App.vue)
 - How components are written and reused. Generally a Vue.js component can have 3 parts - template, script and style. Just get some rough idea about these now and we will learn more later in this session. Note that main UI components are located under src/views folder, and reusable components are located under src/components folder.
 - The project also contains some code about  router, store, and testing. We will focus on these things in the next session.

 ## Exercise
 - In App.vue component, temporarily take way Header (or RouterView) part, run that app and inspect what happens
 - Inspect how HelloWorld component is written and used! For example how properties are defined, and used.
 - Change the color of the title displayed in HelloWorld component to a different color (generated project has green) by modifying style block in the HelloWorld component 
    ```
      .pink {
        color: pink;
      }
      ```
 - Identify a component which is been reused multiple times in the project


## Write your own first Vue.js component
Now, it´s time to write our own component, and lets start with a simple one!

We will add a LoginView to the application generated above. It will have username and password field and a login/sign-in button.

Ideally, when username and password is provided and sign-in button is pressed, an API call will be made to the server and the user will be redireced to another page (feks Home page) if the login is successful, and so on. 

However, you have not learned API calls yet, therefore we will simplify the scenario here - for example upon sign-in button click just display an alert dialog with relavent messages!

### Add a LoginView.vue
- Add a login view views/LoginView.vue. We have specified some UI elements and purposefully assign ids.

```
<template>
      <div id="loginTitle">
        <label>Please login!</label>
      </div>
      <div id="username">
        <label id="usernameLabel">Username:</label>
        <textarea></textarea>
      </div>
      <div id="password">
        <label id="passwordLabel">Password: </label>
        <textarea></textarea>
        <button id="signinbutton">Sign in</button>
        <label id="loginstatusLabel"></label>
      </div>
  </template>
```

- Lets add a router /login that will bind to the LoginView by modifying ```src/router/index.ts``` such that the default path / is mapped to the new LoginView and HomeView mapping is changed to /home path. We will learn more about router in the next session!

```
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/',
      name: 'login',
      component: LoginView,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
```

- Last but not the least, update the router links menu in src/App.vue as below. Note that we have taken away router link for home screen (<RouterLink to="/home">Home</RouterLink>) as we will show the home screen if only if the login is successful!

```
<RouterLink to="/about">About</RouterLink>
<RouterLink to="/">Login</RouterLink>
```

Then, rerun and inspect the app! The application will run fine but as you see the image below, the formating is quite nice! Lets fix this first.

![Alt text](ugly_login.png)

### Fix formating in a LoginView.vue
Styling of Vue.js components can be done in one combination of the following ways
- Using inline ```:style``` property
- Using usual CSS 
- Using ```<style>``` template syntax

In this example, we will start with ```<style>``` template syntax!

So, first lets start with adding a container element and corresponding style that wraps all the UI elements of LoginView. Then contents of ```views/LoginView.vue``` look like this.

Note that we have used 'grid' display type here. You can also use 'flexbox' if you prefer that. Investigate yourself more about 'grid' vs 'flexbox'. Some references:

 - https://medium.com/insiderengineering/diving-deep-into-css-layouts-grid-vs-flexbox-e32f5a75dd90
 - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_grid_layout
 - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_flexible_box_layout


```
<template>
    <div id="loginContainer">
      <div id="loginTitle">
        <label>Please login!</label>
      </div>
      <div id="username">
        <label id="usernameLabel">Username:</label>
        <textarea></textarea>
      </div>
      <div id="password">
        <label id="passwordLabel">Password: </label>
        <textarea></textarea>
        <button id="signinbutton">Sign in</button>
        <label id="loginstatusLabel"></label>
      </div>
    </div>
  </template>
  
  <style scoped>
  #loginContainer {
    display: grid;
    justify-content: center;
    margin: 40px;
  }
  </style>
```

If you rerun and inspect the app, it will look a bit better! But we are not quite there yet!

Maybe we want to have a little bit bigger login title, properly align username and password labels, fields and sign-in button? This can be achieved by adding additional styled elements to the LoginView as below.

```
#loginTitle {
    font-size: x-large;
    font-weight: bold;
    margin-bottom: 20px;
  }
  
  #username,
  #password {
    display: flex;
    flex-direction: row;
    align-items: center;
    column-gap: 20px;
  }
  
  #usernameLabel,
  #passwordLabel {
    width: 100px;
  }
```

If you now rerun and inspect the app, it will look nicer!

![Alt text](nicer_login.png)

### Adding user interaction and data-binding to LoginView.vue
Earlier, we have first added simple LoginView and then improved its layout! In this section, we will enable LoginView such that an user can interact with it, for example

- enter the username and password
- upon clicking sign-in button, an alert dialog will be shown displaying entered username and password 

We will start making use of 'script' block in the LoginView.vue where some variables and functions will be defined that will be used from UI elements (within 'template' block). 

So, lets add a script block to LoginView.vue file as following. As you can see, it has a method 'handleLoginClick' and data variables - username and password. Note that data bariables are not used yet, we will use them shortly!

```
<script setup lang="ts">

function handleLoginClick() {
    alert("You clicked Sign in button!");
}

</script>

```

Then, lets also add event handler to the sign-in button using so called 'v-on:click' or @click directive, as following.

```
  <button v-on:click="handleLoginClick" id="signinbutton">Sign in</button>
```

or

      <button id="signinbutton" @click="handleLoginClick">Sign in</button>


If you rerun the app and click on the 'sign-in' button, you will see an alert dialog pops-up with the message 'You clicked Sign in button!'.

Congratulations, we have written your first interactive component!

![Alt text](login_with_alert.png)

Next, for the sake of additional user interaction, lets implement the following scenario!
- If the entered in the username is not empty and password is longer than 8 characters, and display alert dialog that login is successful :)
- Otherwise, do not display alert dialog, rather update the 'loginstatusLabel' with 'Invalid credentials' message in red color!

First, we need to define data variables in the components (we have already done that!), and then bind these variables to the UI elements using 'v-model' directives. 

We are adding 2 data variables

- username: represents the username and will be bind to the username UI element/textarea
- passwoord: represents the password and will be bind to the password UI element/textarea

Note also that we want to show message upton login failure. For that we will use a label UI element with id 'loginstatusLabel'. We will write a function 'updateErrorMsg' to do that. This function takes error message as input and populates the label text!

The code for the 'LoginView.vue' component so far look like following

```
<template>
    <div id="loginContainer">
      <div id="loginTitle">
        <label>Please login!</label>
      </div>
      <div id="username">
        <label id="usernameLabel">Username:</label>
        <textarea v-model="username"></textarea>
      </div>
      <div id="password">
        <label id="passwordLabel">Password: </label>
        <textarea v-model="password"></textarea>
        <button id="signinbutton" v-on:click="handleLoginClick">Sign in</button>
        <label id="loginstatusLabel"></label>
      </div>
    </div>
  </template>
  
  <style scoped>
  #loginContainer {
    display: grid;
    justify-content: center;
    margin: 40px;
  }

  #loginTitle {
    font-size: x-large;
    font-weight: bold;
    margin-bottom: 20px;
  }
  
  #username,
  #password {
    display: flex;
    flex-direction: row;
    align-items: center;
    column-gap: 20px;
  }
  
  #usernameLabel,
  #passwordLabel {
    width: 100px;
  }

  #loginstatusLabel {
    color: rgb(189, 0, 0);
  }

  </style>


<script setup lang="ts">
import { ref } from 'vue'

const username = ref('')
const password = ref('')

function handleLoginClick() {
  if(username.length > 1 && password.length > 7) {
    updateErrorMsg("");
    alert("Login is successful!");
  } else {
    updateErrorMsg("Invalid credentials!");
  }
}

function updateErrorMsg(errMsg: string) {
  var errorLabel = document.querySelector("label[id=loginstatusLabel]")
  if(errorLabel != null) errorLabel.textContent = errMsg;
}

</script>
```

Rerun the app, inspect and verify!

> **_NOTE:_**  Refs infer the type from the initial value, for example ```const username = ref('')``` refers that ```username``` variable is String type.

> **_NOTE:_**  In Vue, there are different ways of writing components and accordingly ways of defining component properties/variables and methods/functions! In this course, it is recommended to use Composition API based approach, as it compact and at the same time more flexibility compared to Options API based approach.

## Navigating to the home screen on successful login
It is fairly straight forward to navigate between the screens or routes that are already registered via ```ìndex.ts``` file under ```router``` directory.

First, import the router

```
import router from '@/router'
```

Then use the router for example by replacing the alert statement in 'handleLoginClick' function with this

```
router.push("/home")
```

The user will be redirected to the 'home' screen on sucessful login i.e if  username is not empty and password length is at least 8, and so on. 

Rerun the app, inspect and verify.


![Alt text](login_fail.png)

More on routing will be discussed in the next session!

## Exercises and follow-up reading materials


In this session we have created a simple Vue.js LoginView component. 

Note that in a more complex and production-like scenarions, usually 'form' is used. The form is a kind of container element that collects different types of information, usually from the user.

So, the exercise it to convert the above designed LoginView into a form based component! It is very important to validate the form data before it is further processed, so do that as well.

<details>

<summary>Tips</summary>

You can use vee-validate and yup libraries! Remember to install them via ```npm install yup``` and ```npm install vee-validate```

Example code for LoginView with forms and validation is like this.

```Javascript
   <script setup lang="ts">
import { useForm } from 'vee-validate'
import * as yup from 'yup'

const { errors, handleSubmit, defineField } = useForm({
  validationSchema: yup.object({
    username: yup.string().min(1).required(),
    password: yup.string().min(8).required(),
  }),
})

const [username, usernameAttrs] = defineField('username')
const [password, passwordAttrs] = defineField('password')

const submitLoginForm = handleSubmit((values) => {
  console.log('Submitting...' + JSON.stringify(values, null, 2))
  alert('Login credentials will be checked!')
})
</script>

<template>
  <div id="loginContainer">
    <form @submit="submitLoginForm">
      <div id="loginTitle">
        <label>Please login!</label>
      </div>
      <div id="username">
        <label id="usernameLabel">Username:</label>
        <input type="username" v-model="username" v-bind="usernameAttrs" />
      </div>
      <div id="password">
        <label id="passwordLabel">Password: </label>
        <input type="password" v-model="password" v-bind="passwordAttrs" />
        <button id="signinbutton" type="submit">Sign in</button>
      </div>
    </form>
    <label id="loginstatusLabel">{{ errors.username }}</label>
    <label id="loginstatusLabel">{{ errors.password }}</label>
  </div>
</template>

<style scoped>
#loginContainer {
  display: grid;
  justify-content: center;
  margin: 40px;
}

#loginTitle {
  font-size: x-large;
  font-weight: bold;
  margin-bottom: 20px;
}

#username,
#password {
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 20px;
}

#usernameLabel,
#passwordLabel {
  width: 100px;
}

#loginstatusLabel {
  color: rgb(189, 0, 0);
}
</style>

```

</details>

Some of the popular form frameworks for Vue are vee-validate(https://vee-validate.logaretm.com/v4/), FormKit (https://formkit.com/), Vueform (https://vueform.com/), etc.

Recommended reading on forms and form validation are following

- Vue 3 forms (https://www.vuemastery.com/courses/vue3-forms/)
- Validating Vue 3 Forms (https://www.vuemastery.com/courses/validating-vue3-forms/)


---

# Session II - Routers and Stores

## Router - navigation between pages or views

Earlier, we have briefly seen the navigation between the views -  for example from LoginView to Home upon successful login.

In this section, we will look in to more details and understand the underlying mechanisms.

If you want your components(views) to be navigable from another components, these needs to be registered to a Vue router. Each of such navigable components needs to be registed with unique ids. Such ids are then used for nagivation.

The registration is done using ```createRouter``` method from ```vue-router``` package. 

The registration is done in 'index.ts' file under 'router' directory, using ```createRouter``` method from ```vue-router``` package. An example 'index.ts' file looks like following. Here, three routes are registered with unique name and path. One can register as many routes as needed!


```
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginView
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router

```

The routes can then be invoked either programtically or by user actions such as 'click'! 

In order to invoke a route programatically, one need to have access to a 'router', and then call a 'push' method which takes 'path' as param, like this ```router.push("/path")```.

In order to attach a router path to a clickable link, one can use ```RouterLink```component provided by 'vue-router' package. That 'RouterLink' component can then be inserted/used whereever needed in your UI element. A snippet from 'App.vue' is below. In this example, when an user clicks 'About' link, the user will be redirected to AboutView wich is registered as '/about' route.

```
  <nav>
    <RouterLink to="/about">About</RouterLink>
    <RouterLink to="/">Login</RouterLink>
  </nav>
```

## Stores - shared data in an application
Data variables can be used to any individual Vuejs components, for example 'username', 'password' inside 'script' block in 'LoginView' component.

Properties can also be associated with an individual components using 'defineProps' macro, see reference here - https://vuejs.org/guide/components/props.html.

Note the data variables and props are scoped local to a component!

What if one needs data that are global to the whole app? The answer is the use of stores, it allows you to share a state across components or pages. 

There are a few solutions available for supporting stores in Vuejs application. In this session, we will use a library called 'Pinia' - https://pinia.vuejs.org/.

Scenario: Just for the illustration of use of stores, the user information will be stored in a store when user successfully logs-in. And, when the user navigates to the 'Main' page,the logged-in user information will be retrieved from the application 'store' and displayed right after the existing text 'You did it!', like this! 

![Alt text](you_did_it.png)

So, lets get started...

### Storing information in the store

There is one store (counter.ts) that comes with the generated code. Lets delete that one.

And, create a new file 'userstore.ts'. In this file, we will define a store and save logged-in user information there!. There store can be defined using  'defineStore' method  from "pinia" package. A store contains 'state variables' and 'methods/actions'. The state variables holds state data and actions are used to access or modify state variables.

The content of 'userstore.ts' is following. The state has one variable 'loggedInUser' which is initially set to null. It also has one action/function 'saveUserInStore' that takes 'username' as input and populates 'loggedInUser' state variable.

```
import { defineStore } from 'pinia'

export const useUserStore = defineStore('token', {
  state: () => ({
    loggedInUser: '',
  }),

  actions: {
    async saveUserInStore(username: string) {
      try {
        this.loggedInUser = username
      } catch (err) {
        console.log(err)
      }
    },
  },
})
```

Now, we have defined a store. Lets use it from 'LoginView' and 'HomeView' components.

First, import and use 'useStore' in LoginView, as below. As you can see, 
- we have imported 'useUserStore' from 'userstore.ts'
- then defined (or instantiated) a store variable 'userStore'
- then called a method 'saveUserInStore' on store variable with 'username' as input param

```
<script setup lang="ts">
import router from '@/router';
import { useUserStore } from "../stores/userstore";

var username = ""
var password = ""

const userStore = useUserStore();

function handleLoginClick() {
  if(username.length > 1 && password.length > 7) {
    updateErrorMsg("");
    //alert("You clicked Sign in button!");
    userStore.saveUserInStore(username)
    router.push("/home")
  } else {
    updateErrorMsg("Invalid credentials!");
  }
}

function updateErrorMsg(errMsg: string) {
  var errorLabel = document.querySelector("label[id=loginstatusLabel]")
  if(errorLabel != null) errorLabel.textContent = errMsg;
}

</script>
```

If we re-run and inspect the application now, it will run as usual. Upon successful login, the information about user (username) will be stored in the application store.

In the next section we will read the information stored in the store and display in UI.

### Reading information from the store
Now, We want retrieve the logged-in user information from the application 'store' and displayed right after the existing text 'You did it!'

If you navigate through the applicatin code, you will notice that 'You did it!' text is located in 'HelloWorld.vue' file. Therefore, we will need to import and use 'useStore' from there.

In the 'HelloWorld.vue' file, import the define store variable like this

```
import { useUserStore } from "../stores/userstore";
const userStore = useUserStore();
```

And then add an UI element to display the user information, like this, right under where ```msg``` is displayed.

```
<h1>{{ userStore.loggedInUser }}</h1>
````

Note that we are introducing a new CSS class 'red' here just to display the logged-in user in red color! Final code for 'HelloWorld.vue' looks like following.

```
<script setup lang="ts">
import { useUserStore } from "../stores/userstore";
const userStore = useUserStore();
defineProps<{
  msg: string
}>()
</script>

<template>
  <div class="greetings">
    <h1 class="green">{{ msg }}</h1> 
    <h1 class="red">{{ userStore.loggedInUser }}</h1>
    <h3>
      You’ve successfully created a project with
      <a href="https://vitejs.dev/" target="_blank" rel="noopener">Vite</a> +
      <a href="https://vuejs.org/" target="_blank" rel="noopener">Vue 3</a>. What's next?
    </h3>
  </div>
</template>

<style scoped>
h1 {
  font-weight: 500;
  font-size: 2.6rem;
  position: relative;
  top: -10px;
}

h3 {
  font-size: 1.2rem;
}

.red {
  text-decoration: none;
  color: rgb(186, 0, 189);
  transition: 0.4s;
  padding: 3px;
}

.greetings h1,
.greetings h3 {
  text-align: center;
}

@media (min-width: 1024px) {
  .greetings h1,
  .greetings h3 {
    text-align: left;
  }
}
</style>

```

Rerun and inspect the app! 

### Further readings
You are strongly recommended to go through the following video lessions for more information on router and storage.
- Real world Vue 3, https://www.vuemastery.com/courses/real-world-vue3. Checkout lessons on Vue router essentials, Dynamic routing, etc
- Touring Vue Router, https://www.vuemastery.com/courses/touring-vue-router/



That´s it! 
Congratulations for attending Crash course on Vue.js!















